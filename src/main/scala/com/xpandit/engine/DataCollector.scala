package com.xpandit.engine

import com.xpandit.handlers.files.CVSHandlerFile
import org.apache.spark.sql.{DataFrame, SparkSession}

class DataCollector(ss: SparkSession, handlerFile: CVSHandlerFile) {

  def getUserReviews: DataFrame = {
    handlerFile.handleUserReviews
  }

  def getGooglePlayStore: DataFrame = {
    handlerFile.handleGooglePlayStore
  }

  def saveCSVFile(df: DataFrame, path: String) = {
    handlerFile.writeCSVFile(df, path)
  }

  def saveParquetFile(df: DataFrame, path: String) = {
    handlerFile.writeParquetFile(df, path)
  }
}
