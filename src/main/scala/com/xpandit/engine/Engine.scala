package com.xpandit.engine

import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}

class Engine(ss: SparkSession, dataCollector: DataCollector) {

  def taskPart1 = {

    // Loading User Review into dataframe.
    val dfUserReviews = dataCollector.getUserReviews

    // Casting input data.
    val dfUserReviewCast =
      dfUserReviews
        .withColumn(
          "Sentiment_Polarity",
          col("Sentiment_Polarity").cast("Double")
        )
        .withColumn(
          "Sentiment_Subjectivity",
          col("Sentiment_Subjectivity").cast("Double")
        )

    // Average of the column Sentiment_Polarity grouped by App name and without null values.
    dfUserReviewCast
      .groupBy("App")
      .avg("Sentiment_Polarity")
      .withColumnRenamed(
        "avg(Sentiment_Polarity)",
        "Average_Sentiment_Polarity"
      )
      .na
      .fill(0, Seq("Average_Sentiment_Polarity"))
  }

  def taskPart2 = {

    // Loading all the information about the mobile applications registered in the Google Play Store.
    val dfGooglePlayStoreData: DataFrame = dataCollector.getGooglePlayStore

    // Casting input data.
    val dfGooglePlayStoreDataCast =
      dfGooglePlayStoreData
        .withColumn("Rating", col("Rating").cast("Double"))

    // Rating should be greater or equal to 4.0 sorted in descending order.
    val dfUserRating =
      dfGooglePlayStoreDataCast.na
        .fill(0, Seq("Rating"))
        .where("Rating >= 4.0")
        .orderBy(desc("Rating"))

    // Saving the Dataframe as a CSV (delimiter: "§") named "best_apps.csv".
    dataCollector.saveCSVFile(dfUserRating, "src/main/resources/best_apps.csv")
  }

  def taskPart3 = {

    // Loading all the information about the mobile applications registered in the Google Play Store.
    val dfGooglePlayStoreData: DataFrame = dataCollector.getGooglePlayStore

    // Transforming input data.
    val dfMobileDataHandled = prepareMobileData(dfGooglePlayStoreData)

    // Generating a new dataframe order by max number of reviews.
    organizeMobileData(dfMobileDataHandled)
  }

  def taskPart4(dfMobileDataAgg: DataFrame,
                dfAvgSentimentPolarity: DataFrame) = {

    // Producing a Dataframe with all its information plus its 'Average_Sentiment_Polarity.
    val dfCleanedPlayStoreData =
      dfMobileDataAgg.join(dfAvgSentimentPolarity, Seq("App"), "left")

    // Saving the Dataframe as a parquet file with gzip compression with the name "googleplaystore_cleaned".
    dataCollector.saveParquetFile(
      dfCleanedPlayStoreData,
      "src/main/resources/googleplaystore_cleaned"
    )

    dfCleanedPlayStoreData
  }

  def taskPart5(dfCleanedPlayStoreData: DataFrame) = {

    // Generating a new dataframe containing the number of applications, the average rating and the average sentiment
    // polarity by genre.
    val dfSplitGenre = dfCleanedPlayStoreData
      .withColumn("Genre", arrays_zip(col("Genres")))
      .withColumn("Genre", explode(col("Genre")))
      .withColumn(
        "Rating",
        when(col("Rating").isNaN || col("Rating").isNull, 0)
          .otherwise(col("Rating"))
          .as("Rating")
      )
      .select(
        col("Genre.Genres"),
        col("App"),
        col("Rating"),
        col("Average_Sentiment_Polarity")
      )
      .where("Rating > 0")

    val dfPlayStoreMetrics = dfSplitGenre
      .groupBy(col("Genres"))
      .agg(
        count(col("App")) as "Count",
        round(avg(col("Rating")), 5) as "Average_Rating",
        round(avg(col("Average_Sentiment_Polarity")), 4) as "Average_Sentiment_Polarity"
      )

    // Saving the dataframe as a parquet file with gzip compression with the name "googleplaystore_metrics".
    dataCollector.saveParquetFile(
      dfPlayStoreMetrics,
      "src/main/resources/googleplaystore_metrics"
    )
  }

  def prepareMobileData(dfGooglePlayStoreData: DataFrame) = {

    val dfMobileDataTransformed =
      dfGooglePlayStoreData
        .withColumn("Rating", col("Rating").cast("Double"))
        .withColumn("Reviews", col("Reviews").cast("Long"))
        // Convert from string to double (value in MB). Attention - Not all values end in "M"
        .withColumn(
          "Size",
          col("Size")
            .substr(lit(1), length(col("Size")) - 1)
            .cast("Double")
        )
        // Convert from string to double and present the value in euros (All values are in dollars)
        // (Consider conversion rate: 1$ = 0.9€)
        .withColumn(
          "Price",
          round(
            col("Price")
              .substr(lit(2), length(col("Price")))
              .cast("Double")
              .multiply(0.9),
            2
          )
        )
        .na
        .fill(0, Seq("Price"))
        // Convert string to array of strings (delimiter: ";")
        .withColumn("Genres", split(col("Genres"), ";"))
        .withColumn(
          "Last Updated",
          to_date(col("Last Updated"), "MMMMM d, yyyy").cast("date")
        )

    // Renaming columns
    dfMobileDataTransformed
      .withColumnRenamed("Category", "Categories")
      .withColumnRenamed("Content Rating", "Content_Rating")
      .withColumnRenamed("Last Updated", "Last_Updated")
      .withColumnRenamed("Current Ver", "Current_Ver")
      .withColumnRenamed("Android Ver", "Minimum_Android_Version")
  }

  def organizeMobileData(dfMobileData: DataFrame) = {

    dfMobileData
      .withColumn(
        "Categories",
        array_distinct(
          collect_list("Categories")
            .over(Window.partitionBy("App").orderBy("Reviews"))
        )
      )
      .withColumn(
        "Rank",
        row_number().over(
          Window
            .partitionBy(col("App"))
            .orderBy(col("Reviews").desc)
        )
      )
      .filter(col("Rank") === 1)
      .drop(col("Rank"))
  }
}
