package com.xpandit.settings

import com.typesafe.config.ConfigFactory

object Settings {
  private val config = ConfigFactory.load()

  private val hadoopSettings = config.getConfig("hadoop_settings")
  private val weblogGen = config.getConfig("spark_recruitment_challenge")

  lazy val appName: String = weblogGen.getString("appName")
  lazy val logLevel: String = weblogGen.getString("logLevel")
  lazy val codec: String = weblogGen.getString("codec")

  //Hadoop
  lazy val winUtils: String = hadoopSettings.getString("winUtils")
}
