package com.xpandit.handlers.spark

import com.xpandit.settings.Settings
import org.apache.spark.sql.{SQLContext, SparkSession}
import org.apache.spark.{SparkConf, SparkContext}

object Spark {
  val sparkConf: SparkConf = new SparkConf()
  val sparkSession: SparkSession = SparkSession
    .builder()
    .config(sparkConf)
    .appName(Settings.appName)
    .getOrCreate()

  def getSparkSession: SparkSession = {
    this.sparkSession
  }

  def getSparkContext: SparkContext = {
    this.getSparkSession.sparkContext
  }

  def getSparkSQLContext: SQLContext = {
    this.getSparkSession.sqlContext
  }
}
