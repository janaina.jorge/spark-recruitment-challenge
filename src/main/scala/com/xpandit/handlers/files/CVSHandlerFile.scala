package com.xpandit.handlers.files

import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, SparkSession}

class CVSHandlerFile(ss: SparkSession) {

  def loadCVSFile(schema: StructType, path: String): DataFrame = {

    ss.read
      .format("csv")
      .option("header", "true")
      .option("mode", "DROPMALFORMED")
      .schema(schema)
      .load(path)
  }

  def writeCSVFile(df: DataFrame, path: String) = {

    df.coalesce(1)
      .write
      .option("header", "true")
      .option("sep", "§")
      .mode("overwrite")
      .csv(path)
  }

  def writeParquetFile(df: DataFrame, path: String) = {

    df.coalesce(1)
      .write
      .mode("overwrite")
      .parquet(path)
  }

  def handleUserReviews: DataFrame = {

    // Generate input data schema
    val userReviewSchema: StructType = StructType(
      List(
        StructField("App", StringType, true),
        StructField("Translated_Review", StringType, true),
        StructField("Sentiment", StringType, true),
        StructField("Sentiment_Polarity", StringType, true),
        StructField("Sentiment_Subjectivity", StringType, true)
      )
    )

    // Load User Review data into dataframe
    val dfUserReview = loadCVSFile(
      userReviewSchema,
      "src/main/resources/googleplaystore_user_reviews.csv"
    )

    dfUserReview
  }

  def handleGooglePlayStore: DataFrame = {

    // Generate input data schema
    val googlePlayStoreSchema: StructType = StructType(
      List(
        StructField("App", StringType, true),
        StructField("Category", StringType, true),
        StructField("Rating", StringType, true),
        StructField("Reviews", StringType, true),
        StructField("Size", StringType, true),
        StructField("Installs", StringType, true),
        StructField("Type", StringType, true),
        StructField("Price", StringType, true),
        StructField("Content Rating", StringType, true),
        StructField("Genres", StringType, true),
        StructField("Last Updated", StringType, true),
        StructField("Current Ver", StringType, true),
        StructField("Android Ver", StringType, true),
      )
    )

    // Load information about mobile application registered into dataframe
    loadCVSFile(googlePlayStoreSchema, "src/main/resources/googleplaystore.csv")
  }

}
