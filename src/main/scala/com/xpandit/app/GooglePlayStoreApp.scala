package com.xpandit.app

import com.xpandit.engine.{DataCollector, Engine}
import com.xpandit.handlers.files.CVSHandlerFile
import com.xpandit.handlers.spark.Spark
import com.xpandit.settings.Settings
import org.apache.spark.SparkContext
import org.apache.spark.sql.{DataFrame, SQLContext, SparkSession}

import scala.util.{Failure, Try}

object GooglePlayStoreApp {
  val sc: SparkContext = Spark.getSparkContext
  val sqlContext: SQLContext = Spark.getSparkSQLContext
  val ss: SparkSession = Spark.getSparkSession
  val handlerFile: CVSHandlerFile = new CVSHandlerFile(ss)
  val dataCollector: DataCollector = new DataCollector(ss, handlerFile)
  val engine = new Engine(ss, dataCollector)

  sc.setLogLevel(Settings.logLevel)
  sqlContext.setConf("spark.sql.parquet.compression.codec", Settings.codec)

  def main(args: Array[String]): Unit = {

    val main =
      Try {
        val dfPart1: DataFrame = engine.taskPart1
        engine.taskPart2
        val dfPart3: DataFrame = engine.taskPart3
        val dfPart4: DataFrame = engine.taskPart4(dfPart3, dfPart1)
        engine.taskPart5(dfPart4)
      }

    main match {
      case Failure(exception) => {
        println("Execution error ")
        exception.printStackTrace()
        System.exit(1)
      }
      case _ => println("Successful execution")
    }
  }

}
